#ifndef LONG_MATH
#define LONG_MATH

#include "long_int.hpp"

#define BYTE_SIZE_512 64
#define BYTE_SIZE_256 32

Long_int<512> extend (Long_int<256>&);
Long_int<1024> extend (Long_int<512>&);
Long_int<512> add (Long_int<512>&);
Long_int<512> sub (Long_int<512>&);
Long_int<512> mult (Long_int<512>&);
Long_int<512> inv (Long_int<512>&);
Long_int<512> reverse (Long_int<512>&);


#endif