#ifndef LONG_INT
#define LONG_INT
#include <stdint.h>
#include <string>
#include <iostream>
#include <vector>

#define BYTE_SIZE 8

template <int N>
class Long_int {
private:
	uint8_t number[N/BYTE_SIZE] = {0};
public:
	Long_int ();
	Long_int (std::string);
	Long_int (uint64_t);
	Long_int (uint8_t*, int);
	template <int M> Long_int (std::vector < Long_int<M> >&);
	template <int M> std::vector < Long_int<M> > separate ();
	Long_int operator + (Long_int&);
	Long_int& operator ++ ();
	Long_int operator ^ (Long_int&);
	Long_int operator * (bool);
	bool operator == (Long_int&);
	bool operator != (Long_int&);
	bool operator < (Long_int&);
	bool operator > (Long_int&);
	bool operator <= (Long_int&);
	bool operator >= (Long_int&);
	uint8_t get_byte (int);
	void set_byte (uint8_t, int);
};

#endif