#include "declaration.hpp"
#include <fstream>

int main(int argc, char** argv){
	if ((argc < 2) || (argc > 3)){
		std::cerr << "incorrect function call. Sample is < genpkey [-s] file >" << std::endl;;
		return -1;
	}
	int key_size = 64;
	std::string file_name = argv[1];
	if (!strcmp("-s", argv[1]))
	{
		if (argc == 2) 
		{
			std::cerr << "incorrect function call. Sample is < genpkey [-s] file >" << std::endl;;
			return -1;
		}
		key_size = 32;
		file_name = argv[2];
	} else {
		if (argc == 3) 
		{
			std::cerr << "incorrect function call. Sample is < genpkey [-s] file >" << std::endl;;
			return -1;
		}
	}
	std::ofstream output_file;
	output_file.open(file_name, std::ios::binary);
	if (!output_file.is_open())
	{
		std::cerr << file_name << ": No such file or directory" << std::endl;
		return -1;
	}
	Long_int<512> long_int1(35);
	Long_int<512> long_int2(88);
	Long_int<512> long_int3(10);
	Long_int<512> res = mod(long_int1, long_int2);
	ParamSet paramset(MODE_256);
	PrivateKey priv_key(paramset);
	Long_int<512> tmp = priv_key.get_private_key();
	std::cout << tmp << std::endl;
	Contex con;
	con.init(paramset);
	con.sign(priv_key, file_name);
	PublicKey pub_key(paramset, priv_key);
	tmp = pub_key.get_public_key().get_x();
	std::cout << tmp << std::endl; 
	bool check = con.verify(pub_key, file_name);
	if (check) std::cout << "signature is correct" << std::endl;
	else std::cout << "incorrect signature" << std::endl;
	output_file.close();
	return 0;
}