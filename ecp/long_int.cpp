#include "long_int.hpp"

template <int N>
Long_int<N>::Long_int(){
	for (int i = 0; i < N/BYTE_SIZE; i++){
		number[i] = 0x01;
	}
}

template <int N>
Long_int<N>::Long_int (std::string str){
	if (str.size() > N/BYTE_SIZE){
		std::cerr << "The string you are putting is too long" << std::endl;
	}
	for (int i = 0; i < str.size(); i++){
		number[i] = str[i];
	}
	for (int i = str.size(); i < N/BYTE_SIZE; i++){
		number[i] = 0x01;
	}
}

template <int N>
Long_int<N>::Long_int (uint64_t num){
	uint8_t *nums;
	nums = (uint8_t*)num;
	for (int i = 0; i < 8; i++){
		number[i] = nums[i];
	}
}

template <int N>
Long_int<N>::Long_int (uint8_t* arr, int size){
	for (int i = 0; i < size; i++){
		this->number[i] = arr[i];
	}
	for (int i = size; i < N/BYTE_SIZE; i++){
		this->number[i] = 0x01;
	}

}

template <int N>
template <int M>
Long_int<N>::Long_int (std::vector < Long_int<M> > &vec){
	if (N%M != 0){
		std::cerr << "WRONG PARAMETR" << std::endl;
	}
	for (int i = 0; i < N/M; i++){
		for (int j = 0; j < M/BYTE_SIZE; j++){
			number[i*M/BYTE_SIZE+j] = vec[i].get_byte(j);
		}
	}
}

template <int N>
Long_int<N> make_block (uint8_t *num, int i){
	uint8_t res[N/BYTE_SIZE] = {0};
	for (int j = 0; j < N/BYTE_SIZE; j++){
		res[j] = num[j + N/BYTE_SIZE*i];
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
template <int M> 
std::vector < Long_int<M> > Long_int<N>::separate (){
	std::vector < Long_int<M> > vec;
	if (N%M != 0){
		std::cerr << "WRONG PARAMETR" << std::endl;
	}
	for (int i = 0; i < N/M; i++){
		Long_int<M> long_int = make_block<M>(number, i);
	}
	return vec;
}

template <int N>
uint8_t Long_int<N>::get_byte (int i){
	return number[i];
}

template <int N>
void Long_int<N>::set_byte(uint8_t byte, int i){
	this->number[i] = byte;
}

template <int N>
Long_int<N> Long_int<N>::operator + (Long_int<N> &long_int){
	uint8_t res [N/BYTE_SIZE];
	uint8_t flag = 0;
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = (flag + long_int.get_byte(i) + this->number[i])%256;
		flag = (flag + long_int.get_byte(i) + this->number[i])/256;
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
Long_int<N> &Long_int<N>::operator ++ (){
	Long_int<N> L1(1);
	*this = *this + L1;
	return *this;
}

template <int N>
Long_int<N> Long_int<N>::operator ^ (Long_int<N> &long_int){
	Long_int<N> res;
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res.number[i] = this->number[i] ^ long_int.number[i];
	}
	return res;
}

template <int N>
Long_int<N> Long_int<N>::operator * (bool bit){
	if (bit == false) return Long_int<N>(0);
	return *this;
}

template <int N>
bool Long_int<N>::operator == (Long_int<N> &long_int){
	int i = 0;
	while (i != N/BYTE_SIZE){
		if (this->number[i] != long_int.number[i]){
			return false;
		}
		i++;
	}
	return true;
}

template <int N>
bool Long_int<N>::operator != (Long_int<N> &long_int){
	return (!(*this == long_int));
}

template <int N>
bool Long_int<N>::operator < (Long_int<N> &long_int){
	for (int i = N/BYTE_SIZE-1; i > 0; i--){
		if (this->number[i] < long_int.number[i]) return true;
		if (this->number[i] > long_int.number[i]) return false;
	}
	return false;
}

template <int N>
bool Long_int<N>::operator > (Long_int<N> &long_int){
	return ((!(this < long_int) && (this != long_int)));
}

template <int N>
bool Long_int<N>::operator <= (Long_int<N> &long_int){
	return (((this < long_int) || (this == long_int)));
}

template <int N>
bool Long_int<N>::operator >= (Long_int<N> &long_int){
	return (((this > long_int) || (this == long_int)));
}




