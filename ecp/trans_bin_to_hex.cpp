#include <iostream>
#include <fstream>

int main(int argc, char **argv){
	std::string file_name = argv[1];
	std::string str;
	std::ifstream output_file;
	output_file.open(file_name, std::ios::binary);
	std::cout << std::hex;
	output_file >> str;
	for (int i = 0; i < str.size(); i++){
		int j = str.size()-i-1;
		if (str[j] < 0x10) std::cout << 0 << (int)str[j];
		else std::cout << (int)str[j];
	}
	std::cout << std::endl;
	std::cout << std::dec;
	output_file.close();
}