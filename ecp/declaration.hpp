#include <stdint.h>
#include <string>
#include <iostream>
#include <vector>
#include <utility>
#include <fstream>
#include "rand.c"
#include "hash.hpp"

#define BYTE_SIZE 8
#define SIZE_ERR 1
enum Mode {
	MODE_512, MODE_256
};

/* ОПИСАНИЕ КЛАССА LONG_INT */

template <int N>
class Long_int {
private:
	uint8_t number[N/BYTE_SIZE] = {0};
public:
	Long_int ();
	Long_int (std::string, bool dec = true);
	Long_int (uint64_t);
	Long_int (uint8_t*, int);
	template <int M> Long_int (std::vector < Long_int<M> >&);
	template <int M> std::vector < Long_int<M> > separate ();
	Long_int operator + (Long_int&);
	Long_int& operator ++ ();
	Long_int operator ^ (Long_int&);
	Long_int operator * (bool);
	bool operator == (Long_int&);
	bool operator != (Long_int&);
	bool operator < (Long_int&);
	bool operator > (Long_int&);
	bool operator <= (Long_int&);
	bool operator >= (Long_int&);
	uint8_t get_byte (int);
	bool get_bit(int);
	void set_byte (uint8_t, int);
	int get_size ();
	uint8_t* get_number();
	template <int M> friend std::ostream& operator << (std::ostream&, Long_int<M>&);
};

template <int N>
std::ostream& operator << (std::ostream& stream, Long_int<N> &long_int){
	int j = 0;
	stream << std::hex;
	for (int i = 0; i < N/BYTE_SIZE; i++){
		j = N/BYTE_SIZE - i - 1;
		if (long_int.number[j] < 0x10) stream << 0 << (int)long_int.number[j];
		else stream << (int)long_int.number[j];
	}
	stream << std::dec;
	return stream;
}

template <int N>
Long_int<N>::Long_int(){
	for (int i = 0; i < N/BYTE_SIZE; i++){
		number[i] = 0x0;
	}
}

template <int N>
Long_int<N>::Long_int (std::string str, bool dec){
	if (dec){
	if (str.size() > N/BYTE_SIZE){
		throw SIZE_ERR;
	}
	for (int i = 0; i < str.size(); i++){
		number[i] = str[i];
	}
	for (int i = str.size(); i < N/BYTE_SIZE; i++){
		number[i] = 0x0;
		}
	} else {
		if (str.size() > N/4){
			throw SIZE_ERR;
		}
		int j = 0;
		while (!str.empty()){
			auto num = str.substr(0,2);
			str.erase(0,2);
			number[N/BYTE_SIZE - j - 1] = (uint8_t)std::stoi(num,0,16);
			j++;
		}
	}
}

template <int N>
Long_int<N>::Long_int (uint64_t num){
	uint8_t *nums;
	nums = (uint8_t*)&num;
	for (int i = 0; i < 8; i++){
		number[i] = nums[i];
	}
}

template <int N>
Long_int<N>::Long_int (uint8_t* arr, int size){
	for (int i = 0; i < size; i++){
		this->number[i] = arr[i];
	}
	for (int i = size; i < N/BYTE_SIZE; i++){
		this->number[i] = 0x0;
	}

}

template <int N>
template <int M>
Long_int<N>::Long_int (std::vector < Long_int<M> > &vec){
	if (N%M != 0){
		std::cerr << "WRONG PARAMETR" << std::endl;
	}
	for (int i = 0; i < N/M; i++){
		for (int j = 0; j < M/BYTE_SIZE; j++){
			number[i*M/BYTE_SIZE+j] = vec[i].get_byte(j);
		}
	}
}

template <int N>
Long_int<N> make_block (uint8_t *num, int i){
	uint8_t res[N/BYTE_SIZE] = {0};
	for (int j = 0; j < N/BYTE_SIZE; j++){
		res[j] = num[j + N/BYTE_SIZE*i];
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
template <int M> 
std::vector < Long_int<M> > Long_int<N>::separate (){
	std::vector < Long_int<M> > vec;
	if (N%M != 0){
		std::cerr << "WRONG PARAMETR" << std::endl;
	}
	for (int i = 0; i < N/M; i++){
		Long_int<M> long_int = make_block<M>(number, i);
		vec.push_back(long_int);
	}
	return vec;
}

template <int N>
uint8_t Long_int<N>::get_byte (int i){
	return number[i];
}

template<int N>
bool Long_int<N>::get_bit(int i){
	int pos = 7 - i%BYTE_SIZE;
	return (number[i/BYTE_SIZE] & (1 << (sizeof(number[i/BYTE_SIZE])*8-1-pos))) != 0;
}

template <int N>
void Long_int<N>::set_byte(uint8_t byte, int i){
	this->number[i] = byte;
}

template <int N>
int Long_int<N>::get_size(){
	for (int i = 0; i < N/BYTE_SIZE; i++){
		if (this->get_byte(N/BYTE_SIZE-1-i) != 0){
			return N/BYTE_SIZE - i;
		}
	}
	return 0;
}

template <int N>
uint8_t* Long_int<N>::get_number(){
	return number;
}

template <int N>
Long_int<N> Long_int<N>::operator + (Long_int<N> &long_int){
	uint8_t res [N/BYTE_SIZE];
	uint8_t flag = 0;
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = (flag + long_int.get_byte(i) + this->number[i])%256;
		flag = (flag + long_int.get_byte(i) + this->number[i])/256;
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
Long_int<N> &Long_int<N>::operator ++ (){
	Long_int<N> L1(1);
	*this = *this + L1;
	return *this;
}

template <int N>
Long_int<N> Long_int<N>::operator ^ (Long_int<N> &long_int){
	Long_int<N> res;
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res.number[i] = this->number[i] ^ long_int.number[i];
	}
	return res;
}

template <int N>
Long_int<N> Long_int<N>::operator * (bool bit){
	if (bit == false) return Long_int<N>(0);
	return *this;
}

template <int N>
bool Long_int<N>::operator == (Long_int<N> &long_int){
	int i = 0;
	while (i != N/BYTE_SIZE){
		if (this->number[i] != long_int.number[i]){
			return false;
		}
		i++;
	}
	return true;
}

template <int N>
bool Long_int<N>::operator != (Long_int<N> &long_int){
	return (!(*this == long_int));
}

template <int N>
bool Long_int<N>::operator > (Long_int<N> &long_int){
	for (int i = 0; i < N/BYTE_SIZE; i++){
		if (this->number[N/BYTE_SIZE-i-1] > long_int.number[N/BYTE_SIZE-i-1]) return true;
		if (this->number[N/BYTE_SIZE-i-1] < long_int.number[N/BYTE_SIZE-i-1]) return false;
	}
	return false;
}

template <int N>
bool Long_int<N>::operator < (Long_int<N> &long_int){
	return (!(*this > long_int) && (*this != long_int));
}

template <int N>
bool Long_int<N>::operator <= (Long_int<N> &long_int){
	return (((*this < long_int) || (*this == long_int)));
}

template <int N>
bool Long_int<N>::operator >= (Long_int<N> &long_int){
	return (((*this > long_int) || (*this == long_int)));
}

/* ОПИСАНИЕ КЛАССА LONG_INT ЗАВЕРШЕНО*/

/* ОПИСАНИЕ ФУНКЦИЙ ДЛЯ ДЛИННОЙ ЛОГИКИ */

template <int N>
Long_int<2*N> extend (Long_int<N> long_int){
	uint8_t res[2*N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = long_int.get_byte(i);
	}
	return Long_int<2*N>(res, 2*N/BYTE_SIZE);
}

template <int N>
Long_int<N> put_out (Long_int<N> long_int, int beg, int end){
	Long_int<N> res(0);
	for (int i = beg; i <= end; i++){
		res.set_byte(long_int.get_byte(i), i);
	}
	return res;
}

template <int N>
Long_int<N> shiftl (Long_int<N> long_int, int sh){
	auto number = long_int.get_number();
	uint8_t res [2*N/BYTE_SIZE] = {0};
	for (int i = 0; i < 2*N/BYTE_SIZE - sh; i++){
		res[i+sh] = long_int.get_byte(i);
	}
	return Long_int<N>(res, 2*N/BYTE_SIZE);
}

template <int N>
void swap (Long_int<N> &long_int1, Long_int<N> long_int2, int beg, int end){
	for (int i = beg; i <= end; i++){
		long_int1.set_byte(long_int2.get_byte(i),i);
	}
}

template <int N>
Long_int<N> add (Long_int<N> long_int1, Long_int<N> long_int2){
	uint16_t flag = 0;
	uint8_t res[N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = (uint8_t)(((uint16_t)long_int1.get_byte(i) + (uint16_t)long_int2.get_byte(i) + flag)%256);
		flag = (((uint16_t)long_int1.get_byte(i) + (uint16_t)long_int2.get_byte(i) + flag)/256);
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
Long_int<N> sub (Long_int<N> long_int1, Long_int<N> long_int2){
	uint8_t flag = 0;
	uint8_t res[N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = (uint8_t)(long_int1.get_byte(i) - long_int2.get_byte(i) - flag);
		if (long_int1.get_byte(i) < (long_int2.get_byte(i)+flag)) flag = 1;
		else flag = 0;
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
Long_int<N> mult (Long_int<N> long_int, uint8_t mult){
	uint8_t flag = 0;
	uint8_t res [N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = (uint8_t)((long_int.get_byte(i)*mult+flag)%256);
		flag = (uint8_t)((long_int.get_byte(i)*mult+flag)/256);
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
Long_int<N> div (Long_int<N> long_int, uint8_t del){
	uint8_t flag = 0;
	uint8_t res [N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[N/BYTE_SIZE - i - 1] = (uint8_t)((long_int.get_byte(N/BYTE_SIZE - i - 1) + (uint32_t)flag*256)/del);
		flag = (uint8_t)((long_int.get_byte(N/BYTE_SIZE - i - 1) + (uint32_t)flag*256)%del);
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

Long_int<1024> mult (Long_int<512> long_int1, Long_int<512> long_int2){
	uint8_t tmp;
	uint32_t cur;
	uint8_t carry = 0;
	Long_int<1024> res(0);
	int size_1 = long_int1.get_size();
	int size_2 = long_int2.get_size();
	for (int i = 0; i < size_1; i++){
		carry = 0;
		for (int j = 0; j < size_2 || carry; j++){
			if (j < size_2) tmp = long_int2.get_byte(j); 
			else tmp = 0;
			cur = res.get_byte(i+j)+long_int1.get_byte(i)*tmp+carry;
			res.set_byte((uint8_t)(cur%256),i+j);
			carry = cur/256;
		}
	}
	return res;
}

std::pair< Long_int<1024>, Long_int<1024> > div (Long_int<1024> dividend, Long_int<1024> divider)
{
    Long_int<1024> dividend_ext = dividend;
    Long_int<1024> quoitient;
    int dividend_size = dividend.get_size();
    int divider_size = divider.get_size();
    int j = dividend_size - divider_size;
    uint8_t norm = 256 / (divider.get_byte(divider_size - 1) + 1);
    dividend_ext = mult(dividend, norm);
    Long_int<1024> divider_ext = mult(divider, norm);
    while (j >= 0) {
        uint32_t tmp = (uint32_t)dividend_ext.get_byte(j + divider_size) * 256 + (uint32_t)dividend_ext.get_byte(j + divider_size - 1);
        uint32_t q = tmp / divider_ext.get_byte(divider_size - 1);
        uint32_t r = tmp % divider_ext.get_byte(divider_size - 1);
        do {
            if (q == 256 || q * divider_ext.get_byte(divider_size - 2) > 256 * r + dividend_ext.get_byte(j + divider_size - 2)) {
                --q;
                r += divider_ext.get_byte(divider_size - 1);
            } else {
                break;
            }
        } while (r < 256);
        auto tmp1 = put_out(dividend_ext, j, j + divider_size);
        auto tmp_divider = shiftl(divider_ext, j);
        auto mult_2 = mult(tmp_divider, (uint8_t)q);
        while (tmp1 < mult_2) {
            --q;
            mult_2 = mult(tmp_divider, (uint8_t)q);
        }
        tmp1 = sub(tmp1, mult_2);
        swap(dividend_ext, tmp1, j, j + divider_size);
        quoitient.set_byte((uint8_t)q, j);
        --j;
    }
    return std::pair<Long_int<1024>, Long_int<1024> >(quoitient, div(dividend_ext, norm)); 
}

Long_int<1024> power (Long_int<512> p, uint64_t i){
	if (i == 0) return Long_int<1024>(1);
	if (i == 1) return extend<512>(p);
	if (i%2 == 1) {
		Long_int<1024> temp = power(p,i-1);
		Long_int<512> temp_2 = temp.separate<512>()[0];
		return mult(temp_2,p);
	}
	Long_int<1024> temp = power(p, i/2);
	Long_int<512> temp_2 = temp.separate<512>()[0];
	return mult(temp_2, temp_2);
}

template<int N>
Long_int<N> modulus (Long_int<N> li, Long_int<N> m){
	Long_int<N> temp(0);
	Long_int<N> temp_2(0);
	Long_int<N> temp_3(0);
	Long_int<N> temp_4(0);
	Long_int<N> temp_5(0);
	for (uint8_t i = uint8_t(255); i > 1; i--){
		temp = mult<N>(m,i);
		for (uint8_t j = uint8_t(255); j > 1; j--){
			temp_2 = mult<N>(temp,j);
			temp_3 = mult<N>(temp_2,j);
			temp_4 = mult<N>(temp_3,j);
			temp_5 = mult<N>(temp_4,j);
			while (li >= temp_5){
				li = sub<N>(li,temp_5);
				//std::cout << li << std::endl;
			}
		}
	}
	while (li >= m){
		li = sub<N>(li,m);
	}
	return li;
}

uint64_t euler_func (uint64_t num){
	uint64_t res = num;
	for (uint64_t j = 2; j*j <= num; j++){
		if (num % j == 0){
			while (num % j == 0) num/=j;
			res -= res/j;
		}
	}
	if (num > 1) res -= res/num;
	return res;
}

Long_int<512> inv (Long_int<512> num, Long_int<512> modulus){
	Long_int<1024> zero(0);
    Long_int<1024> r0 = extend(modulus);
    Long_int<1024> r1 = div(extend(num), r0).second;
    Long_int<512> p[3];
    Long_int<512> q[3];
    p[0] = 1;
    auto ds = div(r0, r1);
    p[1] = ds.first.separate<512>()[0];
    q[1] = p[1];
    r0 = r1;
    r1 = ds.second;
    int i = 1;
    size_t cnt = 1;
    while (r1 != zero) {
        i = (i + 1) % 3;
        ++cnt;
        auto div_st = div(r0, r1);
        q[i] = div_st.first.separate<512>()[0];
        auto tmp = mult(q[i], p[(3 + i - 1) % 3]);
        p[i] = add(tmp.separate<512>()[0], p[(3 + i - 2) % 3]);
        r0 = r1;
        r1 = div_st.second;
    }
    if (cnt % 2) {
        return p[(3 + i - 1) % 3].separate<512>()[0];
    } else {
        auto result = sub(modulus, p[(3 + i - 1) % 3]);
        return result.separate<512>()[0];
    }   
}


/*Long_int<512> inv (Long_int<512> long_int, Long_int<512> mod){ //для взаимно простых
	uint8_t pow = mod.get_byte(0);
	pow = euler_func(pow) - 0x01;
	Long_int<1024> temp = power(long_int, (int)pow);
	Long_int<1024> mod_ex = extend<512>(mod);
	temp = modulus(temp, mod_ex);
	Long_int<512> temp_2 = temp.separate<512>()[0];
	temp_2 = modulus(temp_2, mod);
	Long_int<512> res = temp_2;
	for (int i = 1; i < 64; i++){
		pow = mod.get_byte(i);
		pow = euler_func(pow);
		temp = power(long_int, (int)pow);
		temp = modulus(temp, mod_ex);
		temp_2 = temp.separate<512>()[0];
		temp_2 = modulus(temp_2, mod);
		temp = mult(temp_2,res);
		res = temp.separate<512>()[0];
	}
	return res;
}*/
/*Long_int<512> del (Long_int<512> long_int1, Long_int<512> long_int2, Long_int<512> mod){
	long_int2 = modulus(long_int2, mod);
	long_int1 = modulus(long_int1, mod);

	Long_int<512> divider = inv(long_int2, mod);
	Long_int<1024> quot = mult(divider, long_int1);
	std::cout << quot << std::endl;
	Long_int<1024> mod_ex = extend<512>(mod);
	quot = modulus(quot, mod_ex);
	return quot.separate<512>()[0];
}*/

Long_int<1024> del (Long_int<1024> dividend, Long_int<1024> divider){
	return div(dividend,divider).first;
}

Long_int<1024> mod (Long_int<1024> dividend, Long_int<1024> divider){
	auto d = div(dividend, divider);
	Long_int<1024> ost = d.second;
	return ost;
}

Long_int<512> del (Long_int<512> dividend, Long_int<512> divider){
	return del(extend(dividend), extend(divider)).separate<512>()[0];
}

Long_int<512> mod (Long_int<512> dividend, Long_int<512> divider){
	return mod(extend(dividend), extend(divider)).separate<512>()[0];
}


Long_int<512> add (Long_int<512> long_int1, Long_int<512> long_int2, Long_int<512> module){
	Long_int<1024> li1 = extend<512>(long_int1);
	Long_int<1024> li2 = extend<512>(long_int2);
	Long_int<1024> res = add<1024>(li1, li2);
	Long_int<1024> mod_ex = extend<512>(module);
	res = mod(res, mod_ex);
	return res.separate<512>()[0];
}

Long_int<512> sub (Long_int<512> long_int1, Long_int<512> long_int2, Long_int<512> module){
	Long_int<512> res = sub<512>(module, long_int2);
	res = add(res, long_int1, module);
	return res;
}

Long_int<512> mult (Long_int<512> long_int1, Long_int<512> long_int2, Long_int<512> module){
	Long_int<1024> res = mult(long_int1, long_int2);
	Long_int<1024> mod_ex = extend<512>(module);
	Long_int<512> res_small(0);
	if (res.get_size() >= 512) res = mod(res, mod_ex); 
	else {
		res_small = mod(res.separate<512>()[0], module);
		return res_small;
	}
	return res.separate<512>()[0];
}


/* ОПИСАНИЕ КЛАССА ELLIPTIC_CURVE*/

class ParamSet {
public:
	Mode mode;
	Long_int<512> p; //модуль эллиптической кривой
	Long_int<512> a; //коэффициенты уравнения в канонической форме
	Long_int<512> b;
	Long_int<512> e; //коэффициенты уравнения в форме Эдвардса
	Long_int<512> d;
	Long_int<512> m; //порядок группы точек 
	Long_int<512> q; //порядок циклической подгруппы группы точек
	Long_int<512> x; //координата точки кривой в канонической форме
	Long_int<512> y;
	Long_int<512> u; //координата точки кривой в форме Эдвардса
	Long_int<512> v;
	ParamSet(Mode m = MODE_512);
	Mode get_mode();
};

class Point {
private:
	Long_int<512> x;
	Long_int<512> y;
	ParamSet *params;
public:
	Point();
	Point(ParamSet*);
	Point(Long_int<512>&, Long_int<512>&, ParamSet*);
	Point(int, int, ParamSet*);
	Point operator+(Point&);
	//Point operator*(Long_int<512>&, Point&);
	Point to_veir();
	ParamSet* get_params();
	Long_int<512> get_x();
	Long_int<512> get_y();
	friend std::ostream& operator << (std::ostream&, Point&);
};

std::ostream& operator << (std::ostream& stream, Point& p){
	Long_int<512> temp;
	stream << "x = ";
	temp = p.get_x();
	stream << temp << std::endl;
	stream << "y = ";
	temp = p.get_y();
	stream << temp << std::endl;
	return stream;
}

ParamSet::ParamSet(Mode mod):mode(mod){
	if (mod == MODE_512){
		p = Long_int<512>("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDC7", false);
		a = Long_int<512>("DC9203E514A721875485A529D2C722FB187BC8980EB866644DE41C68E143064546E861C0E2C9EDD92ADE71F46FCF50FF2AD97F951FDA9F2A2EB6546F39689BD3", false);
		b = Long_int<512>("B4C4EE28CEBC6C2C8AC12952CF37F16AC7EFB6A9F69F4B57FFDA2E4F0DE5ADE038CBC2FFF719D2C18DE0284B8BFEF3B52B8CC7A5F5BF0A3C8D2319A5312557E1", false);
		e = 1;
		d = Long_int<512>("9E4F5D8C017D8D9F13A5CF3CDF5BFE4DAB402D54198E31EBDE28A0621050439CA6B39E0A515C06B304E2CE43E79E369E91A0CFC2BC2A22B4CA302DBB33EE7550", false);
		m = Long_int<512>("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF26336E91941AAC0130CEA7FD451D40B323B6A79E9DA6849A5188F3BD1FC08FB4", false);
		q = Long_int<512>("3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC98CDBA46506AB004C33A9FF5147502CC8EDA9E7A769A12694623CEF47F023ED", false);
		x = Long_int<512>("E2E31EDFC23DE7BDEBE241CE593EF5DE2295B7A9CBAEF021D385F7074CEA043AA27272A7AE602BF2A7B9033DB9ED3610C6FB85487EAE97AAC5BC7928C1950148", false);
		y = Long_int<512>("F5CE40D95B5EB899ABBCCFF5911CB8577939804D6527378B8C108C3D2090FF9BE18E2D33E3021ED2EF32D85822423B6304F726AA854BAE07D0396E9A9ADDC40F", false);
		u = 0x12;
		v = Long_int<512>("469AF79D1FB1F5E16B99592B77A01E2A0FDFB0D01794368D9A56117F7B38669522DD4B650CF789EEBF068C5D139732F0905622C04B2BAAE7600303EE73001A3D", false);
	} else {
		Long_int<256> temp_p = Long_int<256>("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD97",false);
		p = extend<256>(temp_p);
		Long_int<256> temp_a = Long_int<256>("C2173F1513981673AF4892C23035A27CE25E2013BF95AA33B22C656F277E7335", false);
		a = extend(temp_a);
		Long_int<256> temp_b = Long_int<256>("295F9BAE7428ED9CCC20E7C359A9D41A22FCCD9108E17BF7BA9337A6F8AE9513", false);
		b = extend(temp_b);
		e = 1;
		Long_int<256> temp_d = Long_int<256>("0605F6B7C183FA81578BC39CFAD518132B9DF62897009AF7E522C32D6DC7BFFB", false);
		d = extend(temp_d);
		m = Long_int<512>("0000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000003F63377F21ED98D70456BD55B0D8319C", false);
		Long_int<256> temp_q = Long_int<256>("400000000000000000000000000000000FD8CDDFC87B6635C115AF556C360C67", false);
		q = extend(temp_q);
		Long_int<256> temp_x = Long_int<256>("91E38443A5E82C0D880923425712B2BB658B9196932E02C78B2582FE742DAA28", false);
		x = extend(temp_x);
		Long_int<256> temp_y = Long_int<256>("32879423AB1A0375895786C4BB46E9565FDE0B5344766740AF268ADB32322E5C", false);
		y = extend(temp_y);
		u = 0x0D;
		Long_int<256> temp_v = Long_int<256>("60CA1E32AA475B348488C38FAB07649CE7EF8DBE87F22E81F92B2592DBA300E7", false);
		v = extend(temp_v);
	}
}

Mode ParamSet::get_mode(){
	return mode;
}

Point::Point(){

}

Point::Point(ParamSet *param_set){
	x = param_set->u;
	y = param_set->v;
	params = param_set;
}

Point::Point(int a, int b, ParamSet* p):Point(p){
	x = Long_int<512>((uint64_t)a);
	y = Long_int<512>((uint64_t)b);
	params = p;
}

Point::Point(Long_int<512>&a, Long_int<512>&b, ParamSet* p):Point(p){
	x = a;
	y = b;
	params = p;
}

ParamSet* Point::get_params(){
	return params;
}

Long_int<512> Point::get_x(){
	return x;
}

Long_int<512> Point::get_y(){
	return y;
}

Point Point::operator+ (Point& point){
	Long_int<512> one(1);
	Long_int<512> mod = params->p;
	Long_int<512> m1 = mult(x, point.y, mod); //x1y2
	Long_int<512> m2 = mult(point.x, y, mod); //x2y1
	Long_int<512> a = add(m1, m2, mod); //x1y2 + x2y1
	Long_int<512> m3 = mult(m1, m2, mod); //x1x2y1y2
	Long_int<512> m4 = mult(m3, params->d, mod); //dx1x2y1y2
	Long_int<512> b = add(one,m4,mod); //1+dx1x2y1y2
	Long_int<512> m5 = inv(b, mod);
	Long_int<512> n_x = mult(a,m5,mod);
	m1 = mult(y, point.y, mod); //y1y2
	m2 = mult(x, point.x, mod); //x1x2
	m3 = mult(params->e, m2, mod); //ex1x2
	a = sub(m1, m3, mod); //y1y2-ex1x2
	b = sub(one, m4, mod); //1-dx1x2y1y2
	m5 = inv(b,mod);
	Long_int<512> n_y = mult(a,m5,mod);
	return Point(n_x,n_y, params);
}

Point Point::to_veir (){
	Long_int<512> mod = params->p;
	Long_int<512> temp_2(6);
	Long_int<512> temp(4);
	Long_int<512> temp_3(1);
	Long_int<512> s = sub(params->e, params->d, mod); //e-d
	Long_int<512> m4 = inv(temp,mod); //1/4
	s = mult(s, m4, mod); //(e-d)/4
	Long_int<512> t = add(params->e, params->d, mod); //e+d
	m4 = inv(temp_2,mod);
	t = mult(t,m4, mod); //e+d/6
	Long_int<512> m1 = add(temp_3, y, mod); //1+v
	Long_int<512> a = mult(s,m1,mod); //s(1+v)
	Long_int<512> b = sub(temp_3, y, mod); //1-v
	Long_int<512> m5 = inv(b,mod);
	Long_int<512> m2 = mult(a,m5,mod); 
	Long_int<512> n_x = add(m2,t,mod);
	Long_int<512> m3 = mult(b,x,mod);
	m3 = inv(m3, mod);
	Long_int<512> n_y = mult(a,m3,mod);
	return Point(n_x, n_y, params);
}

Point mult (Long_int<512> n, Point p){
	Point arr [512];
	arr[0] = p;
	Long_int<512> zero(0);
	Long_int<512> one(1);
	Point res(0, 1, p.get_params());
	for (int i = 1; i < 512; i++){
		arr[i] = arr[i-1]+arr[i-1];
	}
	for (int i = 0; i < 512; i++){
		if (n.get_bit(i)){
			res = res+arr[i];
		}
	}
	return res;
}

/* ЗАВЕРШЕНИЕ ОПИСАНИЯ КЛАССА ELLIPTIC_CURVE*/

/* ОПИСАНИЕ КЛАССОВ КЛЮЧЕЙ*/

class PrivateKey{
private:
	Long_int<512> priv_k;
	ParamSet *param_set;
public:
	PrivateKey(ParamSet&);
	Long_int<512> get_private_key();
};

class PublicKey{
private:
	ParamSet *param_set;
	Point pub_k;
public:
	PublicKey(ParamSet&, PrivateKey&);
	Point get_public_key();
};

Long_int<512> gen_priv_key(ParamSet &p){
	Long_int<512> res_512(0);
	Long_int<256> res_256(0);
	Long_int<512> zero_512(0);
	Long_int<256> zero_256(0);
	uint8_t arr[64] = {0};
	int arr_size;
	if (p.get_mode() == MODE_512) arr_size = 64;
	else arr_size = 32;
	if (arr_size == 64) {
		do{
			rand_bytes(arr,arr_size);
			res_512 = Long_int<512>(arr, arr_size);
		} while ((res_512 == zero_512) || (res_512 >= p.q));
		return res_512;
	}
	else {
		Long_int<256> param_q = p.q.separate<256>()[0];
		do{
			rand_bytes(arr, arr_size);
			res_256 = Long_int<256>(arr, arr_size);
		} while ((res_256 == zero_256) || (res_256 >= param_q));
	}
	return extend<256>(res_256);
}

PrivateKey::PrivateKey(ParamSet &p){
	param_set = &p;
	priv_k = gen_priv_key(p);
}

Long_int<512> PrivateKey::get_private_key(){
	return priv_k;
}

PublicKey::PublicKey(ParamSet &param, PrivateKey &priv_key) : pub_k(0, 1, &param){
	Point temp(&param);
	Long_int<512> priv_k = priv_key.get_private_key();
	pub_k = mult(priv_k, temp);
}

Point PublicKey::get_public_key(){
	return pub_k;
}

/* ЗАВЕРШЕНИЕ ОПИСАНИЯ КЛАССОВ КЛЮЧЕЙ*/

class Contex {
private:
	ParamSet *param_set;
	Long_int<512> hash;
	Long_int<512> r;
	Long_int<512> s;
public:
	void init(ParamSet&);
	void sign(PrivateKey&, std::string &);
	bool verify(PublicKey&, std::string &);
};

std::string reverse (std::string str){
	std::string new_str;
	for (int i = 0; i < str.size(); i++){
		new_str+=str[str.size()-i-1];
	}
	return new_str;
}


void Contex::init(ParamSet &param){
	param_set = &param;
	if (param.get_mode() == MODE_512){
		hash = Long_int<512>("486f64c1917879417fef082b3381a4e211c324f074654c38823a7b76f830ad00fa1fbae42b1285c0352f227524bc9ab16254288dd6863dccd5b9f54a1ad0541b", false);
	} else {
		Long_int<256> temp_hash("7713db8c932644a00ff742b893bb04d63b90e33992817dbd9959ef36dc99a1ae", false);
		hash = extend<256>(temp_hash);
	}
}

void Contex::sign(PrivateKey& priv_key, std::string &file_name){
	std::ofstream output_file;
	output_file.open(file_name, std::ios::binary);
	Long_int<512> k;
	Long_int<512> e = mod(hash, param_set->q);
	Long_int<512> zero(0);
	if (e == zero) e = 1;
	do {
		k = priv_key.get_private_key();
		Point p(param_set);
		Point c = mult(k,p);
		r = mod(c.to_veir().get_x(), param_set->q);
	} while (r == zero);
	Long_int<512> rd = mult(r, priv_key.get_private_key(), param_set->q);
	Long_int<512> ke = mult(k, e, param_set->q);
	Long_int<512> s = add(rd, ke, param_set->q);
	if (param_set->get_mode() == MODE_256){
		output_file << zero.separate<256>()[0] << s.separate<256>()[0] << std::endl;
	} else {
		output_file << r << s << std::endl;
	}
}

bool Contex::verify(PublicKey &pub_key, std::string &file_name){
	std::ifstream input_file;
	input_file.open(file_name, std::ios::binary);
	std::string str;
	Mode m = param_set->get_mode();
	std::getline(input_file, str);
	if (m == MODE_256){
		Long_int<256> r_temp(str.substr(0,64),false);
		Long_int<256> s_temp(str.substr(64,str.size()),false);
		r = extend(r_temp);
		s = extend(s_temp);
	} else {
		r = Long_int<512>(str.substr(0,128),false);
		s = Long_int<512>(str.substr(128,str.size()),false);
	}
	Long_int<512> e = mod(hash, param_set->q);
	Long_int<512> zero(0);
	if (e == zero) e = 1;
	Long_int<512> v = inv(e,param_set->q);
	Long_int<512> z1 = mult(s,v,param_set->q);
	Long_int<512> z2 = mult(r,v,param_set->q);
	z2 = sub(zero, z2, param_set->q);
	Point c1 = mult(z1, Point(param_set));
	Point c2 = mult(z2,pub_key.get_public_key());
	Point c = c1 + c2;
	Long_int<512> big_r = mod(c.to_veir().get_x(), param_set->q);
	if (r == big_r) return true;
	else return false;
}































