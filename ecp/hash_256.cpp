#include <iostream>
#include <vector>
#include <bitset>
#include <cmath>
#include <fstream>
#include <string>
#include <time.h>


class contex{
public:
	std::vector<bool> h;
	std::vector<bool> N;
	std::vector<bool> sigma;
	std::vector<bool> K[13];
	size_t size;
	std::string mes; //читаем блоками по 64 байта
};

std::ifstream keyfile;

char hex[16] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

std::string C[12] = { 
	"b1085bda1ecadae9ebcb2f81c0657c1f2f6a76432e45d016714eb88d7585c4fc4b7ce09192676901a2422a08a460d31505767436cc744d23dd806559f2a64507",
	"6fa3b58aa99d2f1a4fe39d460f70b5d7f3feea720a232b9861d55e0f16b501319ab5176b12d699585cb561c2db0aa7ca55dda21bd7cbcd56e679047021b19bb7",
	"f574dcac2bce2fc70a39fc286a3d843506f15e5f529c1f8bf2ea7514b1297b7bd3e20fe490359eb1c1c93a376062db09c2b6f443867adb31991e96f50aba0ab2",
	"ef1fdfb3e81566d2f948e1a05d71e4dd488e857e335c3c7d9d721cad685e353fa9d72c82ed03d675d8b71333935203be3453eaa193e837f1220cbebc84e3d12e",
	"4bea6bacad4747999a3f410c6ca923637f151c1f1686104a359e35d7800fffbdbfcd1747253af5a3dfff00b723271a167a56a27ea9ea63f5601758fd7c6cfe57",
	"ae4faeae1d3ad3d96fa4c33b7a3039c02d66c4f95142a46c187f9ab49af08ec6cffaa6b71c9ab7b40af21f66c2bec6b6bf71c57236904f35fa68407a46647d6e",
	"f4c70e16eeaac5ec51ac86febf240954399ec6c7e6bf87c9d3473e33197a93c90992abc52d822c3706476983284a05043517454ca23c4af38886564d3a14d493",
	"9b1f5b424d93c9a703e7aa020c6e41414eb7f8719c36de1e89b4443b4ddbc49af4892bcb929b069069d18d2bd1a5c42f36acc2355951a8d9a47f0dd4bf02e71e",
	"378f5a541631229b944c9ad8ec165fde3a7d3a1b258942243cd955b7e00d0984800a440bdbb2ceb17b2b8a9aa6079c540e38dc92cb1f2a607261445183235adb",
	"abbedea680056f52382ae548b2e4f3f38941e71cff8a78db1fffe18a1b3361039fe76702af69334b7a1e6c303b7652f43698fad1153bb6c374b4c7fb98459ced",
	"7bcd9ed0efc889fb3002c6cd635afe94d8fa6bbbebab076120018021148466798a1d71efea48b9caefbacd1d7d476e98dea2594ac06fd85d6bcaa4cd81f32d1b",
	"378ee767f11631bad21380b00449b17acda43c32bcdf1d77f82012d430219f9b5d80ef9d1891cc86e71da4aa88e12852faf417d5d9b21b9948bc924af11bd720"
};

int Pi[256] = { 
	252, 238, 221, 17, 207, 110, 49, 22, 
	251, 196, 250, 218, 35, 197, 4, 77, 
	233, 119, 240, 219, 147, 46, 153, 186, 
	23, 54, 241, 187, 20, 205, 95, 193, 
	249, 24, 101, 90, 226, 92, 239, 33, 
	129, 28, 60, 66, 139, 1, 142, 79,
	5, 132, 2, 174, 227, 106, 143, 160, 
	6, 11, 237, 152, 127, 212, 211, 31, 
	235, 52, 44, 81, 234, 200, 72, 171, 
	242, 42, 104, 162, 253, 58, 206, 204, 
	181, 112, 14, 86, 8, 12, 118, 18, 
	191, 114, 19, 71, 156, 183, 93, 135,
	21, 161, 150, 41, 16, 123, 154, 199, 
	243, 145, 120, 111, 157, 158, 178, 177, 
	50, 117, 25, 61, 255, 53, 138, 126,
	109, 84, 198, 128, 195, 189, 13, 87, 
	223, 245, 36, 169, 62, 168, 67, 201, 
	215, 121, 214, 246, 124, 34, 185, 3, 
	224, 15, 236, 222, 122, 148, 176, 188, 
	220, 232, 40, 80, 78, 51, 10, 74, 
	167, 151, 96, 115, 30, 0, 98, 68, 
	26, 184, 56, 130, 100, 159, 38, 65, 
	173, 69, 70, 146, 39, 94, 85, 47,
	140, 163, 165, 125, 105, 213, 149, 59, 
	7, 88, 179, 64, 134, 172, 29, 247, 
	48, 55, 107, 228, 136, 217, 231, 137, 
	225, 27, 131, 73, 76, 63, 248, 254,
	141, 83,170, 144, 202, 216, 133, 97, 
	32, 113, 103, 164, 45, 43, 9, 91, 
	203, 155, 37, 208, 190, 229, 108, 82,
	89, 166, 116, 210, 230, 244, 180, 192,
	209, 102, 175, 194, 57, 75, 99, 182
};

int T[64] = { 
	0, 8, 16, 24, 32, 40, 48, 56, 
	1, 9, 17, 25, 33, 41, 49, 57, 
	2, 10, 18, 26, 34, 42, 50, 58, 
	3, 11, 19, 27, 35, 43, 51, 59, 
	4, 12, 20, 28, 36, 44, 52, 60, 
	5, 13, 21, 29, 37, 45, 53, 61, 
	6, 14, 22, 30, 38, 46, 54, 62, 
	7, 15, 23, 31, 39, 47, 55, 63
};

std::string A[64] = { 
	"8e20faa72ba0b470", "47107ddd9b505a38", "ad08b0e0c3282d1c", "d8045870ef14980e",
	"6c022c38f90a4c07", "3601161cf205268d", "1b8e0b0e798c13c8", "83478b07b2468764",
	"a011d380818e8f40", "5086e740ce47c920", "2843fd2067adea10", "14aff010bdd87508",
	"0ad97808d06cb404", "05e23c0468365a02", "8c711e02341b2d01", "46b60f011a83988e",
	"90dab52a387ae76f", "486dd4151c3dfdb9", "24b86a840e90f0d2", "125c354207487869",
	"092e94218d243cba", "8a174a9ec8121e5d", "4585254f64090fa0", "accc9ca9328a8950",
	"9d4df05d5f661451", "c0a878a0a1330aa6", "60543c50de970553", "302a1e286fc58ca7",
	"18150f14b9ec46dd", "0c84890ad27623e0", "0642ca05693b9f70", "0321658cba93c138",
	"86275df09ce8aaa8", "439da0784e745554", "afc0503c273aa42a", "d960281e9d1d5215",
	"e230140fc0802984", "71180a8960409a42", "b60c05ca30204d21", "5b068c651810a89e",
	"456c34887a3805b9", "ac361a443d1c8cd2", "561b0d22900e4669", "2b838811480723ba",
	"9bcf4486248d9f5d", "c3e9224312c8c1a0", "effa11af0964ee50", "f97d86d98a327728",
	"e4fa2054a80b329c", "727d102a548b194e", "39b008152acb8227", "9258048415eb419d",
	"492c024284fbaec0", "aa16012142f35760", "550b8e9e21f7a530", "a48b474f9ef5dc18",
	"70a6a56e2440598e", "3853dc371220a247", "1ca76e95091051ad", "0edd37c48a08a6d8",
	"07e095624504536c", "8d70c431ac02a736", "c83862965601dd1b", "641c314b2b8ee083"
};

std::string reverse(std::string str){
	std::string rev_str;
	for (int i = 0; i < str.length(); i++){
		rev_str+=str[str.length()-i-1];
	}
	return rev_str;
}

std::string get_string(std::istream &stream){
	std::string str;
	char c;
	while ((str.length()<64) && (!stream.eof())){
		stream.read((char*)&c,sizeof(char));
		str+=c;
	}
	if (stream.eof()) {
		str.resize(str.length()-1); 
	}
	return reverse(str);
}

void print_boolvect(std::vector<bool> vec){
	for (int i = 0; i < vec.size(); i++){
		std::cout << vec[i];
	}
	std::cout << std::endl;
}

void print_boolvect_16(std::vector<bool> vec){
	std::bitset<4> b(0);
	int j = 0;
	for (int i = 0; i < vec.size(); i++){
		b[3-j] = vec[i];
		j++;
		if (j==4){
			std::cout << hex[b.to_ulong()];
			j = 0;
			b = 0;
		}
	}
}

std::string boolvect_8_to_string(std::vector<bool> vec){
	std::bitset<8> b(0);
	std::string str;
	int j = 0;
	for (int i = 0; i < vec.size(); i++){
		b[7-j] = vec[i];
		j++;
		if (j==8){
			str += char(b.to_ulong());
			j = 0;
			b = 0;
		}
	}
	return str;
}

std::string boolvect_16_to_string(std::vector<bool> vec){
	std::bitset<4> b(0);
	std::string str;
	int j = 0;
	for (int i = 0; i < vec.size(); i++){
		b[3-j] = vec[i];
		j++;
		if (j==4){
			str += hex[b.to_ulong()];
			j = 0;
			b = 0;
		}
	}
	return str;
}

std::vector<bool> add_2 (std::vector<bool> vec1, std::vector<bool> vec2){
	std::vector<bool> res;
	for (int i = 0; i < vec1.size(); i++){
		if (vec1[i] == vec2[i]) res.push_back(0);
		else res.push_back(1);
	}
	return res;
}

int vect_to_num(std::vector<bool> v){
	int ch = 0;
	for (int i = 0; i < v.size(); i++){
		ch+=v[v.size()-1-i]*pow(2,i);
		while (ch > 512) ch -= 512;
		while (ch < 0) ch +=512;
	}
	std::cout << ch << std::endl;
	return ch;
}

std::vector<bool> num_to_vect512(int num){
	std::vector<bool> v;
	std::bitset<512> b(num);
	for (int i = 0; i < 512; i++){
		v.push_back(b[511 - i]);
	}
	return v;
}

std::vector<bool> S (std::vector<bool> v){
	int ch = 0;
	int j = 7;
	std::vector<bool> new_v;
	std::bitset<8> temp;
	for (int i = 0; i < v.size(); i++){
		ch+=v[i]*pow(2,j);
		j--;
		if (j < 0){
			temp = std::bitset<8>(Pi[ch]);
			for (int k = 0; k < 8; k++){
				new_v.push_back(temp[7-k]);
			}
			j = 7;
			ch = 0;
		}
	}
	return new_v;
}

std::vector<bool> P (std::vector<bool> v){
	int t = 0;
	std::vector<bool> new_v;
	for (int i = 0; i < (v.size()/8); i++){
		for (int j = 0; j < 8; j++){
			new_v.push_back(v[8*T[t]+j]);
		}
		t++;
	}
	return new_v;
}

std::bitset<64> string16_to_bit64(std::string str){
	std::bitset<4> temp;
	std::bitset<64> new_b(0);
	int num;
	for (int i = 0; i < str.length(); i++){
		for (int j = 0; j < 16; j++){
			if (str[i] == hex[j]){
				num = j;
				break;
			}
		}
		temp = std::bitset<4>(num);
		for (int j = 0; j < 4; j++){
			new_b[63-i*4-3+j] = temp[j];
		}
	}
	return new_b;
}

std::vector<bool> string_to_bool(std::string str){
	std::bitset<8> tmp;
	std::vector<bool> vec;
	for (int i = 0; i < str.length(); i++){
		tmp = std::bitset<8>(str[i]);
		for (int j = 0; j < 8; j++){
			vec.push_back(tmp[7-j]);
		}
	}
	return vec;
}

std::vector<bool> string_to_bool_16(std::string str){
	std::bitset<4> temp;
	std::vector<bool> new_v;
	int num;
	for (int i = 0; i < str.length(); i++){
		for (int j = 0; j < 16; j++){
			if (str[i] == hex[j]){
				num = j;
				break;
			}
		}
		temp = std::bitset<4>(num);
		for (int j = 0; j < 4; j++){
			new_v.push_back(temp[3-j]);
		}
	}
	return new_v;	
}

std::bitset<64> l (std::bitset<64> b){
	std::bitset<64> new_b;
	if (b[63] != 0) new_b = string16_to_bit64(A[0]);
	else new_b = 0;
	for (int i = 1; i < 64; i++){
		if (b[63-i] == 0){
			new_b ^= string16_to_bit64("");
		} else {
			new_b ^= string16_to_bit64(A[i]);
		}
	}
	return new_b;
}

std::vector<bool> L (std::vector<bool> v){
	int j = 63;
	std::vector<bool> new_v;
	std::bitset<64> temp;
	for (int i = 0; i < v.size(); i++){
		temp[j] = v[i];
		j--;
		if (j < 0){
			temp = l(temp);
			for (int k = 0; k < 64; k++){
				new_v.push_back(temp[63-k]);
			}
			j = 63;
			temp = std::bitset<64>(0);
		}
	}
	return new_v;
}

std::vector<bool> get_iv (int length = 512){
	std::vector<bool> iv;
	switch (length){
		case 512: 
			for (int i = 0; i < 512; i++){
				iv.push_back(0);
			}	
			return iv;
			break;
		case 256:
			for (int i = 0; i < 64; i++){
				for (int j = 0; j < 7; j++){
					iv.push_back(0);
				}
				iv.push_back(1);
			}
			return iv;
			break;
		default:
			std::cout << "Incorrect length. Only 256 or 512 is correct" << std::endl;
			return iv;
	}
}

std::vector<bool> createm (std::vector<bool> M){
	std::vector<bool> m;
	for (int i = 0; i < (511 - M.size()); i++)
	{
		m.push_back(0);
	}
	m.push_back(1);
	for (int i = 0; i < M.size(); i++){
		m.push_back(M[i]);
	}
	return m;
}

std::vector<bool> get_last(std::vector<bool> vec, int count_last = 512){
	std::vector<bool> new_vec;
	for (int i = 0; i < 512; i++){
		new_vec.push_back(vec[vec.size()-512+i]);
	}
	return new_vec;
}

std::vector<bool> get_first(std::vector<bool> vec, int count_last = 512){
	std::vector<bool> new_vec;
	for (int i = 0; i < (vec.size()-count_last); i++){
		new_vec.push_back(vec[i]);
	}
	return new_vec;
}

std::vector<bool> E(std::vector<bool> K, std::vector<bool> m){
	for (int i = 0; i < 12; i++){
		m = L(P(S(add_2(K,m))));
		K = L(P(S(add_2(K, string_to_bool_16(C[i])))));
	}
	m = add_2(K,m);
	return m;
}

std::vector<bool> reverse(std::vector<bool> v){
	std::vector<bool> vfin;
	for (int i = 0; i < v.size(); i++){
		vfin.push_back(v[v.size()-1-i]);
	}
	return vfin;
}

std::vector<bool> add_ring(std::vector<bool> n1, std::vector<bool> n2){
	std::vector<bool> n;
	bool one = 0;
	for (int i = 511; i >= 0; i--){
		if ((n1[i]+n2[i]+one) == 0){
			n.push_back(0);
			continue;
		}
		if ((n1[i]+n2[i]+one) == 1){
			one = 0;
			n.push_back(1);
			continue;
		}
		if ((n1[i]+n2[i]+one) == 2){
			one = 1;
			n.push_back(0);
			continue;
		}
		if ((n1[i]+n2[i]+one) == 3){
			n.push_back(1);
			one = 1;
			continue;
		}
	}
	return reverse(n);
}

void gN(contex *A, std::vector<bool> vec, std::vector<bool> m, std::vector<bool> n){
	A->h = add_2( add_2( E( L(P(S( add_2(A->h, n)))), m),A->h), m);
}

void change_mes(contex *A, std::vector<bool> message){
	std::vector<bool> m = get_last(message);
	gN(A, A->h, m, A->N);
	A->N = add_ring(A->N, num_to_vect512(512));
	A->sigma = add_ring(A->sigma, m);
}

void get_hash(contex *A){
	std::vector<bool> M = string_to_bool(A->mes);
	/*while (M.size() >= 512){
		change_mes(M);
		M = get_first(M);
	}*/
	std::vector<bool> m = createm(M);
	gN(A,A->h,m,A->N);
	A->N = add_ring (A->N, num_to_vect512(M.size()));
	A->sigma = add_ring(A->sigma, m);
	gN(A,A->h, A->N, get_iv());
	gN(A,A->h, A->sigma, get_iv());
	A->h = get_first(A->h, 256);
}

void init(contex *A, int length){
	A->h = get_iv(length);
	A->N = get_iv();
	A->sigma = get_iv();
}

void update(contex *A, std::string str){
	A->size = str.length();
	std::vector<bool> M = string_to_bool(str);
	if (A->size == 64) {
		change_mes(A, M);
		M = get_first(M);
	}
	A->mes = boolvect_8_to_string(M);
	A->size = A->mes.length();
}

void finish(contex *A){
	get_hash(A);
}

int main(int argc, char **argv){
	clock_t start = clock();
	int length = 256;
	keyfile.open(argv[1], std::ios::binary);
	if (keyfile.is_open()) {
		contex HASH;
		init (&HASH, length);
		update(&HASH, get_string(keyfile));
		while ((HASH.size == 0) && (!keyfile.eof())){
			std::cout << "ok" << std::endl;
			update(&HASH, get_string(keyfile));
		};
		finish(&HASH);
		print_boolvect_16(HASH.h);
		keyfile.close();
	} else {
		std::cerr << "g256sum: " << argv[1] << ": No such file or directory" << std::endl;
	} 
	std::cout << std::endl;
	printf("time=%.3lfsec\n", (clock() - start) * 1. / CLOCKS_PER_SEC);
	return 0;
}