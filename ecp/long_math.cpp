#include "long_math.hpp"

/*Long_int<512> extend (Long_int<256> &long_int){
	uint8_t res[BYTE_SIZE_512] = {0};
	for (int i = 0; i < BYTE_SIZE_256; i++){
		res[i] = long_int.get_byte(i);
	}
	return Long_int<512>(res, BYTE_SIZE_512);
}

Long_int<1024> extend (Long_int<512> &long_int){
	uint8_t res[2*BYTE_SIZE_512] = {0};
	for (int i = 0; i < BYTE_SIZE_512; i++){
		res[i] = long_int.get_byte(i);
	}
	return Long_int<1024>(res, 2*BYTE_SIZE_512);
}*/

template <int N>
Long_int<2*N> extend (Long_int<N> &long_int){
	uint8_t res[2*N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = long_int.get_byte(i);
	}
	return Long_int<2*N>(res, 2*N/BYTE_SIZE);
}

template <int N>
Long_int<N> add (Long_int<N> &long_int1, Long_int<N> &long_int2){
	uint16_t flag = 0;
	uint8_t res[N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = (uint8_t)(((uint16_t)long_int1.get_byte(i) + (uint16_t)long_int2.get_byte(i) + flag)%256);
		flag = (((uint16_t)long_int1.get_byte(i) + (uint16_t)long_int2.get_byte(i) + flag)/256);
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
Long_int<N> sub (Long_int<N> &long_int1, Long_int<N> &long_int2){
	uint8_t flag = 0;
	uint8_t res[N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = (uint8_t)(long_int1.get_byte(i) - long_int2.get_byte(i) - flag);
		if (long_int1.get_byte(i) < (long_int2.get_byte(i)+flag)) flag = 1;
		else flag = 0;
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
Long_int<N> mult (Long_int<N> &long_int, uint8_t mult){
	uint8_t flag = 0;
	uint8_t res [N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[i] = (uint8_t)((long_int.get_byte(i)*mult+flag)%256);
		flag = (uint8_t)((long_int.get_byte(i)*mult+flag)/256);
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}

template <int N>
Long_int<N> div (Long_int<N> &long_int, uint8_t del){
	uint8_t flag = 0;
	uint8_t res [N/BYTE_SIZE] = {0};
	for (int i = 0; i < N/BYTE_SIZE; i++){
		res[N/BYTE_SIZE - i - 1] = (uint8_t)((long_int.get_byte(N/BYTE_SIZE - i - 1) + (uint32_t)flag*256)/del);
		flag = (uint8_t)((long_int.get_byte(N/BYTE_SIZE - i - 1) + (uint32_t)flag*256)%del);
	}
	return Long_int<N>(res, N/BYTE_SIZE);
}




